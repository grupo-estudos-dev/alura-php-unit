# Alura php unit

## Conceitos de estrutura de testes

__AAA__
* ARRANGE 
* ACT
* ASSERT

__Definição do Martin Fowler__
* GIVEN 
* WHEN 
* THEN



## Fixtrures 
__Para executar código antes ou depois de testes, o PHPUnit nos fornece as fixtures. São métodos que vão ser executados em momentos específicos.__
* public static function *setUpBeforeClass()*: void - Método executado uma vez só, antes de todos os testes da classe
* public function *setUp()*: void - Método executado antes de cada teste da classe
* public function *tearDown()*: void - Método executado após cada teste da classe
* public static function *tearDownAfterClass()*: void - Método executado uma vez só, após todos os testes da classe