<?php

namespace Alura\Leilao;

use Alura\Leilao\Model\Lance;
use Alura\Leilao\Model\Leilao;
use Alura\Leilao\Model\Usuario;
use DomainException;
use PHPUnit\Framework\TestCase;

class LeilaoTest extends TestCase
{

    public function testleilaoNaoDeveReceberLancesRepetidos() 
    {
        $this->expectException(DomainException::class);
        $this->expectExceptionMessage('Usuário não pode propor 2 lances consecutivos');
        $leilao = new Leilao('Variante');
        $ana = new Usuario('Ana');
        $leilao->recebeLance(new Lance($ana, 1000));
        $leilao->recebeLance(new Lance($ana, 2000));
    }
    /**
     * @dataProvider geraLances
     */
    public function testLeilaoDeveReceberLances(int $qtdLances, Leilao $leilao, array $valores)
    {
        static::assertCount($qtdLances, $leilao->getLances());

        foreach ($valores as $key => $valor) {
            static::assertEquals($valor, $leilao->getLances()[$key]->getValor());            
        }
    }

    public function geraLances()
    {        
        $joao  = new Usuario('Joao');
        $maria = new Usuario('Maria');

        $leilaocom2lances = new Leilao('Fiat 147 0km');
        $leilaocom2lances->recebeLance(new Lance($joao, 1000));
        $leilaocom2lances->recebeLance(new Lance($maria, 2000));

        
        $leiaocom1lance = new Leilao('Fusca');
        $leiaocom1lance->recebeLance(new Lance($maria, 5000));

        return [
            '2-lances' => [2, $leilaocom2lances, [1000, 2000]],
            '1-lance' => [1, $leiaocom1lance, [5000]]
        ];
    }

    public function testLeilaoNaoDeveAceitarMaisDeCincoLancesPorUsuario()
    {
        $this->expectException(DomainException::class);
        $this->expectExceptionMessage('Usuário não pode propor mais de 5 lances por leilão');
        $leilao = new Leilao('Brasilia amarela');
        $joao = new Usuario('joao');
        $maria = new Usuario('maria');
        $leilao->recebeLance(new Lance($joao, 1000));
        $leilao->recebeLance(new Lance($maria, 2000));
        $leilao->recebeLance(new Lance($joao, 3000));
        $leilao->recebeLance(new Lance($maria, 4000));
        $leilao->recebeLance(new Lance($joao, 5000));
        $leilao->recebeLance(new Lance($maria, 6000));
        $leilao->recebeLance(new Lance($joao, 7000));
        $leilao->recebeLance(new Lance($maria, 8000));
        $leilao->recebeLance(new Lance($joao, 9000));
        $leilao->recebeLance(new Lance($maria, 10000));
        
        $leilao->recebeLance(new Lance($joao, 9000));
    }
}