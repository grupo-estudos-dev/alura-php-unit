<?php

namespace Alura\Leilao\tests\Service;

use Alura\Leilao\Model\Lance;
use Alura\Leilao\Model\Leilao;
use Alura\Leilao\Model\Usuario;
use PHPUnit\Framework\TestCase;
use Alura\Leilao\Service\Avaliador;
use DomainException;

class AvaliadorTest extends TestCase
{

	private $leiloeiro;

	protected function setUp() : void
	{
		$this->leiloeiro = new Avaliador;
	}

	public function testLeilaoVazioNaoPodeSerAvaliado()
	{
		$this->expectException(DomainException::class);	
		$this->expectExceptionMessage('Não é possivel avaliar leilão vazio.');	
		$leilao = new Leilao('Fusca azul');
		$this->leiloeiro->avalia($leilao);
	}

	/**
	 * @dataProvider entregaLeiloes
	 */
	public function testAvaliadorDeveEncontarOMaiorValorDeLances(Leilao $leilao)
	{
		//Act - When
		$this->leiloeiro->avalia($leilao);

		$maiorValor = $this->leiloeiro->getMaiorValor();

		// Assert - Then
		$this->assertEquals(2000, $maiorValor);
	}

	/**
	 * @dataProvider entregaLeiloes
	 */
	public function testAvaliadorDeveEncontarOMenorValorDeLances(Leilao $leilao)
	{
		// Arrange - given

		//Acty - When
		$this->leiloeiro->avalia($leilao);

		$menorValor = $this->leiloeiro->getmenorValor();

		// Assert - Then
		self::assertEquals(1000, $menorValor);
	}

	/**
	 * @dataProvider entregaLeiloes
	 */
	public function testAvaliadorDeveEncontrar3MaioresValores(Leilao $leilao)
	{
		$this->leiloeiro->avalia($leilao);

		$maiores = $this->leiloeiro->getMaioresLances();
		self::assertCount(3, $maiores);
		self::assertEquals(1000, $maiores[0]->getValor());
		self::assertEquals(1500, $maiores[1]->getValor());
		self::assertEquals(2000, $maiores[2]->getValor());
	}

	public function leilaoEmOrdemCrescente()
	{		
		$leilao = new Leilao('Fiat 147 0km');

		$joao 	= new Usuario('Joao');
		$maria 	= new Usuario('Maria');
		$ana 	= new Usuario('Ana');

		$leilao->recebeLance(new Lance($joao, 1000));
		$leilao->recebeLance(new Lance($ana, 1500));
		$leilao->recebeLance(new Lance($maria, 2000));

		return $leilao;
	}

	public function leilaoEmOrdemDecrescente()
	{
		$leilao = new Leilao('Fiat 147 0km');

		$joao 	= new Usuario('Joao');
		$maria 	= new Usuario('Maria');
		$ana 	= new Usuario('Ana');

		$leilao->recebeLance(new Lance($maria, 2000));
		$leilao->recebeLance(new Lance($ana, 1500));
		$leilao->recebeLance(new Lance($joao, 1000));

		return $leilao;
	}

	public function leilaoEmOrdemAleatoria()
	{
		$leilao = new Leilao('Fiat 147 0km');

		$joao 	= new Usuario('Joao');
		$maria 	= new Usuario('Maria');
		$ana 	= new Usuario('Ana');

		$leilao->recebeLance(new Lance($ana, 1500));
		$leilao->recebeLance(new Lance($maria, 2000));
		$leilao->recebeLance(new Lance($joao, 1000));

		return $leilao;
	}

	public function entregaLeiloes()
	{
		return [
			'ordem-crescente' => [$this->leilaoEmOrdemCrescente()],
			[$this->leilaoEmOrdemDecrescente()],
			[$this->leilaoEmOrdemAleatoria()]
		];
	}

	public function testLeilaoFinalizadoNaoPodeSerAvaliado()
	{
		$this->expectException(DomainException::class);
		$this->expectExceptionMessage('Leilão já finalizado');
		$leilao = new Leilao('Fiat 147 0km');
		$leilao->recebeLance(new Lance(new Usuario('Teste'), 1500));
		$leilao->finaliza();
		$this->leiloeiro->avalia($leilao);

	}
}
